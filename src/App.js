import './App.css';
import Video from 'twilio-video';
import { useState } from 'react';

function OnRoomJoined(room) {
  console.log('Successfully joined to a Room: ', room);
  room.participants.forEach(participant => {
    participant.tracks.forEach(publication => {
      if (publication.track) {
        document.getElementById('remote-media-div').appendChild(publication.track.attach());
      }
    });  
    participant.on('trackSubscribed', track => {
      document.getElementById('remote-media-div').appendChild(track.attach());
    });
  });
}

function RemoteParticipantConnected(participant) {
  console.log(`Participant "${participant.identity}" has connected to the Room`);

  participant.tracks.forEach(publication => {
    if (publication.isSubscribed) {
      const track = publication.track;
      document.getElementById('remote-media-div').appendChild(track.attach());
    }
  });

  participant.on('trackSubscribed', track => {
    document.getElementById('remote-media-div').appendChild(track.attach());
  });
}

// https://www.twilio.com/docs/video/javascript-v2-getting-started#join-a-room
function logRoomState(room) {
  OnRoomJoined(room);

  // Log your Client's LocalParticipant in the Room
  const localParticipant = room.localParticipant;
  console.log(`Connected to the Room as LocalParticipant "${localParticipant.identity}"`);

  // Log any Participants already connected to the Room
  room.participants.forEach(participant => {
    console.log(`Participant "${participant.identity}" is connected to the Room`);
  });

  // Log new Participants as they connect to the Room
  room.once('participantConnected', participant => RemoteParticipantConnected(participant));

  // Log Participants as they disconnect from the Room
  room.once('participantDisconnected', participant => {
    console.log(`Participant "${participant.identity}" has disconnected from the Room`);
  });
}

async function connect(roomName, participant, setLoadingMsg) {
  setLoadingMsg('Creating room...');
  try {
    let response = await fetch(`http://localhost:3000/room/initialize/${roomName}/${participant}`);
    let token = await response.text();
    // setLoadingMsg(response.headers["token-auth"]);
    setLoadingMsg(token);
    let room = await Video.connect(token, { name: roomName });
    logRoomState(room);
  }
  catch (e) {
    console.error('Unable to connect to Room', e);
    setLoadingMsg(JSON.stringify(e));
  }
}

function App() {
  const [roomName, setRoomName] = useState('SalesMeeting');
  const [participant, setParticipant] = useState('Juan');
  const [msg, setMessage] = useState('');
  const [token, setToken] = useState('');
  return (
    <div className="App">
      <header className="App-header">
        <div className="content">
          <div>
            Room name: <input id="roomName" value={roomName} onChange={e => setRoomName(e.target.value)} />
          </div>
          <div>
            Participant: <input id="participant" value={participant} onChange={e => setParticipant(e.target.value)} />
          </div>
          <div>
            <button onClick={() => connect(roomName, participant, setMessage)}>Join to room</button>
            <p>{msg}</p>
          </div>
        </div>
        <div id="remote-media-div"></div>
        <div>
          <textarea value={token} onChange={e => setToken(e.target.value)}></textarea>
          <br/>
          <button disabled={token === ""} onClick={() => {
            setMessage(token);
            Video.connect(token, { name: roomName })
            .then(room => logRoomState(room));
          }}>Connect with token</button>          
        </div>
      </header>
    </div>
  );
}

export default App;
